package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * add by shanghuaichao
 *
 * @date 2019/5/28 下午2:46
 */
@RestController
public class Hello {
    @RequestMapping("/")
    public String index() {
        return "Hello";
    }
}
